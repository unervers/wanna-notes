import sbt._
name := "wanna-notes"

version := "0.1"

scalaVersion := "2.13.6"

val zioVersion = "1.0.8"

val http4sVersion = "0.21.24"


val circeVersion = "0.14.0"


libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % zioVersion,
  "dev.zio" %% "zio-interop-cats" % "2.5.1.0",

  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "com.github.pureconfig" %% "pureconfig" % "0.15.0",

  "org.slf4j" % "slf4j-log4j12" % "1.7.30",
)
